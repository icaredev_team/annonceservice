package com.icare.annonce_service.wrapper;

import com.icare.annonce_service.model.Annonce;
import com.icare.annonce_service.model.Trajet;

/**
 * Created by ahadri on 1/20/17.
 */
public class AnnonceWrapper {

	private Long conducteurId;
	private Trajet trajet;
	private Annonce annonce;

	public AnnonceWrapper() {
	}

	public AnnonceWrapper(Long conducteurId, Trajet trajet, Annonce annonce) {
		this.conducteurId = conducteurId;
		this.trajet = trajet;
		this.annonce = annonce;
	}


	public Long getConducteurId() {
		return conducteurId;
	}

	public void setConducteurId(Long conducteurId) {
		this.conducteurId = conducteurId;
	}

	public Trajet getTrajet() {
		return trajet;
	}

	public void setTrajet(Trajet trajet) {
		this.trajet = trajet;
	}

	public Annonce getAnnonce() {
		return annonce;
	}

	public void setAnnonce(Annonce annonce) {
		this.annonce = annonce;
	}



	@Override
	public String toString() {
		return "AnnonceWrapper{" +
					   "conducteurId=" + conducteurId +
					   ", trajet=" + trajet.toString() +
					   ", annonce=" + annonce.toString() +
					   '}';
	}
}
