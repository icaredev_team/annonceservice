package com.icare.annonce_service.wrapper;

/**
 * Created by ahadri on 1/25/17.
 */
public class CreateDemandeWrapper {

	private Long annonceId;
	private Long clientId;

	public CreateDemandeWrapper(Long annonceId, Long clientId) {
		this.annonceId = annonceId;
		this.clientId = clientId;
	}

	public CreateDemandeWrapper() {
	}

	public Long getAnnonceId() {
		return annonceId;
	}

	public void setAnnonceId(Long annonceId) {
		this.annonceId = annonceId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	@Override
	public String toString() {
		return "CreateDemandeWrapper{" +
					   "annonceId=" + annonceId +
					   ", clientId=" + clientId +
					   '}';
	}
}
