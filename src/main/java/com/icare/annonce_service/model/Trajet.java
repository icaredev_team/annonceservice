package com.icare.annonce_service.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Created by ahadri on 1/20/17.
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Trajet {

	@GeneratedValue
	@Id
	private Long id;

	@OneToOne(mappedBy = "trajet")
	@JsonIgnore
	private Annonce annonce;


	private String depart;
	private String destination;



	public Trajet() {
	}

	public Trajet(String depart, String destination) {
		this.depart = depart;
		this.destination = destination;
	}

	@Override
	public String toString() {
		return "Trajet{" +
					   "id=" + id +
					   ", annonce=" + annonce +
					   ", depart='" + depart + '\'' +
					   ", destination='" + destination + '\'' +
					   '}';
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDepart() {
		return depart;
	}

	public void setDepart(String depart) {
		this.depart = depart;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Annonce getAnnonce() {
		return annonce;
	}

	public void setAnnonce(Annonce annonce) {
		this.annonce = annonce;
	}
}
