package com.icare.annonce_service.model;

import javax.persistence.*;


/**
 * Created by ahadri on 1/24/17.
 */

@Entity
public class Demande {

	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "clientId")
	private Client client;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "annonceId")
	private Annonce annonce;

	@Column(columnDefinition = "boolean default false", nullable = false)
	private boolean status;

	public Demande() {
	}

	@Override
	public String toString() {
		return "Demande{" +
					   "id=" + id +
				//	   ", client=" + client +
					   ", annonce=" + annonce +
					   ", status=" + status +
					   '}';
	}

	public Demande(Client client, Annonce annonce) {
		this.client = client;
		this.annonce = annonce;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Annonce getAnnonce() {
		return annonce;
	}

	public void setAnnonce(Annonce annonce) {
		this.annonce = annonce;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}
