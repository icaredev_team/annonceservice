package com.icare.annonce_service;

import com.icare.annonce_service.dao.AnnonceRepository;
import com.icare.annonce_service.dao.ClientRepository;
import com.icare.annonce_service.dao.DemandeRepository;
import com.icare.annonce_service.model.Annonce;
import com.icare.annonce_service.model.Client;
import com.icare.annonce_service.model.Demande;
import com.icare.annonce_service.wrapper.CreateAnnonceWrapper;
import com.icare.annonce_service.wrapper.CreateDemandeWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@SpringBootApplication
@EnableDiscoveryClient
@EnableBinding(MessageStream.class)
@RestController
public class AnnonceServiceApplication {

	@Autowired
	private AnnonceRepository annonceRepository;
	@Autowired
	private DemandeRepository demandeRepository;
	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private MessageStream messageStream;



	@StreamListener(MessageStream.ADD_ANNONCE_INPUT)
	@SendTo(MessageStream.ADD_ANNONCE_OUTPUT)
	public Annonce createAnnonce(CreateAnnonceWrapper createAnnonceWrapper) {
		return annonceRepository.createAnnonce(createAnnonceWrapper);
	}


	@RequestMapping(method = RequestMethod.GET, value = "/annonces/{annonceId}")
	public Annonce getAnnonceById(@PathVariable("annonceId") Long annonceId) {
		return annonceRepository.findOne(annonceId);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/annonces")
	public List<Annonce> getAllAnnonces() {
		return annonceRepository.findAll();
	}


	@RequestMapping(method = RequestMethod.GET, value = "/annonces/{annonceId}/demandes")
	public Set<Demande> getAnnonceDemandes(@PathVariable("annonceId") Long annonceId) {
		Annonce annonce = annonceRepository.findOne(annonceId);
		if(annonce != null) {
			return annonce.getDemandes();
		}
		return null;
	}



@RequestMapping(method = RequestMethod.POST, value = "/demandes")
	public boolean createDemande(@RequestBody CreateDemandeWrapper createDemandeWrapper) {
		Annonce annonce = annonceRepository.findOne(createDemandeWrapper.getAnnonceId());
		Long clientId = createDemandeWrapper.getClientId();
		Client client = clientRepository.findOne(clientId);
		if (client == null) {
			client = new Client();
			client.setId(clientId);
		}
		return demandeRepository.createDemande(client, annonce);
	}

	@StreamListener(MessageStream.ACCEPT_DEMANDE_INPUT)
	@SendTo(MessageStream.ACCEPT_DEMANDE_OUTPUT)
	public boolean acceptDemande(Long demandeId) {
		Demande demande = demandeRepository.findOne(demandeId);
		if ( demande != null) {
			demande.setStatus(true);
		}
		return false;
	}

	public static void main(String[] args) {
		SpringApplication.run(AnnonceServiceApplication.class, args);
	}
}






interface MessageStream {
	String ADD_ANNONCE_OUTPUT = "addAnnonceOutput";
	String ADD_ANNONCE_INPUT = "addAnnonceInput";
	@Output(ADD_ANNONCE_OUTPUT)
	SubscribableChannel addAnnonceOutput();
	@Input(ADD_ANNONCE_INPUT)
	SubscribableChannel addAnnonceInput();

	String ACCEPT_DEMANDE_OUTPUT = "acceptDemandeOutput";
	String ACCEPT_DEMANDE_INPUT = "acceptDemandeInput";
	@Output(ACCEPT_DEMANDE_OUTPUT)
	SubscribableChannel acceptDemandeOutput();
	@Input(ACCEPT_DEMANDE_INPUT)
	SubscribableChannel acceptDemandeInput();

}
