package com.icare.annonce_service.dao;

import com.icare.annonce_service.model.Annonce;
import com.icare.annonce_service.model.Client;
import com.icare.annonce_service.model.Demande;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ahadri on 1/25/17.
 */
public interface DemandeRepository extends JpaRepository<Demande, Long> {

	default boolean createDemande(Client client, Annonce annonce) {
		Demande demande = new Demande();
		demande.setAnnonce(annonce);
		demande.setClient(client);
		if( this.save(demande)!= null) {
			client.getDemandes().add(demande);
			return true;
		}
		return false;
	}

}
