package com.icare.annonce_service.dao;

import com.icare.annonce_service.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ahadri on 1/25/17.
 */
public interface ClientRepository extends JpaRepository<Client, Long>{
}
