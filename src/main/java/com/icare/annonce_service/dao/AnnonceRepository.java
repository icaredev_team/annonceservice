package com.icare.annonce_service.dao;

import com.icare.annonce_service.model.Annonce;
import com.icare.annonce_service.wrapper.CreateAnnonceWrapper;
import com.icare.annonce_service.model.Conducteur;
import com.icare.annonce_service.model.Trajet;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by ahadri on 1/20/17.
 */
public interface AnnonceRepository extends JpaRepository<Annonce, Long> {


	  default Annonce createAnnonce(CreateAnnonceWrapper createAnnonceWrapper) {
		Annonce annonce = createAnnonceWrapper.getAnnonce();
		Trajet trajet = createAnnonceWrapper.getTrajet();
		annonce.setTrajet(trajet);
		Conducteur conducteur = new Conducteur();
		conducteur.setId(createAnnonceWrapper.getConducteurId());
		annonce.setConducteur(conducteur);
		return save(annonce);
	}
}
